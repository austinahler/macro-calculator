import { Component, OnInit } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  height: number = null;
  weight: number = null;
  age: number = null;
  activityLevel: number = null;
  result: number = null;
  gender: boolean = null;
  invalid: boolean = false;
  recommended = {
    protein: null,
    carbs: null,
    fats: null
  };
  flag: boolean = false;
  numGrams = {
    protein: null,
    carbs: null,
    fats: null
  }

  calculate() {
    if (this.validate()) {
      if (this.gender) {
        this.calculateMale();
      } else {
        this.calculateFemale();
      }
    }
  }

  validate() {
    if (this.height == null || this.weight == null || this.age == null || this.activityLevel == null || this.gender == null) {
      this.invalid = true;
      return false;
    } else {
      return true;
    }
  }

  reset() {
    this.height = null;
    this.weight = null;
    this.age = null;
    this.activityLevel = null;
    this.gender = null;
    this.result = null;
    this.recommended = null;
    this.flag = false;
    this.invalid = false;
    this.numGrams = null;
  }

  calculateMale() {
    let temp = 10 * (this.weight / 2.2) + 6.25 * (this.height * 2.54) - 5 * this.age + 5;
    this.result = Math.floor(temp * this.activityLevel);
    this.findRecommended();
  }

  calculateFemale() {
    let temp = 10 * (this.weight / 2.2) + 6.25 * (this.height * 2.54) - 5 * this.age - 161;
    this.result = Math.floor(temp * this.activityLevel);
    this.findRecommended();
  }

  findRecommended() {
    this.recommended.protein = Math.floor(this.result * .20);
    this.recommended.carbs = Math.floor(this.result * .55);
    this.recommended.fats = Math.floor(this.result * .25);
    this.findNumOfGrams();
    this.flag = true;
  }

  findNumOfGrams(){
    this.numGrams.protein = Math.floor(this.recommended.protein / 4);
    this.numGrams.carbs = Math.floor(this.recommended.carbs / 4);
    this.numGrams.fats = Math.floor(this.recommended.fats / 9);
  }
}
